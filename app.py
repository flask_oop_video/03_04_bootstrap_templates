from flask import Flask, render_template
app = Flask(__name__)

@app.route("/")
def hello():
    return render_template("home.html")

@app.route("/about")
def about():
    return render_template("about.html")

@app.route("/user/<string:name>")
def user(name):
    return render_template("user.html", name=name)



app.run(debug=True)